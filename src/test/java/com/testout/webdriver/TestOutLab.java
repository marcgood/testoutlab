package com.testout.webdriver;

import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.interactions.*;


public class TestOutLab {
    //Instantiate
    WebDriver driver;
    WebDriverWait wait;
    WebElement slideButton;
    String baseURL;

    @Before
    public void setUp() {
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 2);
    }

    @After
    public void tearDown() {
        driver.close();
        driver.quit();
    }

    @Test
    public void testOutLab(){
        // Define base URL for test
        baseURL = "http://testoutlivecontent.blob.core.windows.net/netpro2018v5-en-us/en-us/sims/typescriptv1/netpro2018v5/simstartup_webpack.html?package=netpro2018v5windowspackage&sim=ipademail_np5&dev=true&automation=true";

        // Maximize the browser
        driver.manage().window().maximize();
        // Navigate to TestOut Lab
        driver.navigate().to(baseURL);
        // Wait until Lab loads
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("bDone.Grid.contentPresenter.TextBlock")));
        // Hold page to show work
        WaitABit();
        // **** Set email account to use SSL and port 993 ****
        // Select Settings
        driver.findElement(By.id("wpDesktop.DesktopIcon15")).click();
        WaitABit();
        // Select Mail, Contacts, Calendars
        driver.findElement(By.id("siMailContactsCalendars")).click();
        WaitABit();
        // Select Maggie Brown
        driver.findElement(By.id("lbAccounts.Grid.Border.ScrollViewer.Grid.Viewport.ScrollContentPresenter.OuterElement.InnerElement.ItemsPresenter.StackPanel.SettingsItemSubMenu.Grid.ContentPresenter.StackPanel.TextBlock")).click();
        WaitABit();
        // Select mbrown@gmail.com
        driver.findElement(By.id("siAccount.Grid.tbStateText")).click();
        WaitABit();
        // Select Advanced
        driver.findElement(By.id("siAdvanced.Grid.rectClickArea")).click();
        WaitABit();
        // Slide the button to enable 'Use SSL'
        enableSSLButton();
        // Verify Server Port is 993
        String portNumber = driver.findElement(By.id("tbServerPort")).getAttribute("value");
        Assert.assertEquals("The port was changed.", "993", portNumber);
        // At top, select Account
        driver.findElement(By.id("btnAccount.grid.contentPresenter.TextBlock")).click();
        WaitABit();
        // Click Done
        driver.findElement(By.id("btnDone.grid.contentPresenter.TextBlock")).click();
        WaitABit();
        // **** Connect to CorpNet-Wireless Wi-Fi  ****
        // Click Wi-Fi
        driver.findElement(By.id("siWiFi.Grid.tbText")).click();
        WaitABit();
        // Click CorpNet
        driver.findElement(By.id("SettingsItemNetwork.Grid.tbText")).click();
        WaitABit();
        // Password = @CorpNetWeRSecure!&
        driver.findElement(By.id("psbx")).sendKeys("@CorpNetWeRSecure!&");
        WaitABit();
        // Click Join
        driver.findElement(By.id("btnJoin.grid.contentPresenter.TextBlock")).click();
        WaitABit();
        // Click Done to get Lab Score
        driver.findElement(By.id("bDone.Grid.contentPresenter.TextBlock")).click();
        WaitABit();
        // Confirm Performance score is 100%
        driver.switchTo().frame(1);
        String performanceScore = driver.findElement(By.cssSelector("#reportDiv > div > div:nth-child(1) > div:nth-child(2)")).getText();
        //System.out.println("Performance Score : " + performanceScore);
        Assert.assertTrue(performanceScore.contains("100%"));

    } // end testOutLab()

    private void enableSSLButton() {
        slideButton = driver.findElement(By.id("siUseSSL.Grid.tbOnOff.Grid.SwitchRoot.Canvas.SwitchThumb.Grid.Border.Grid.Background"));
        Actions builder = new Actions(driver);
        builder.moveToElement(slideButton)
                .click()
                .dragAndDropBy(slideButton, 20, 0)
                .build()
                .perform();
    }

    
    private static void WaitABit() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            System.out.println("Error! :(");
        }
    }
}
